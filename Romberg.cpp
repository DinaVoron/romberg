// Вычисление определенного интеграла методом Ромберга. Функция x^2+2x+1 (выше ОХ)
#include <iostream>
#include <cmath> 
using namespace std;

double f(double num)
{
	return (pow(num, 2) + 2 * num + 1);
}

int main()
{
	double a, b; //границы интегрирования
	double S = 0; //площадь
	//double h; //счетчик длины
	double h; //шаг
	int precision; //точность
	cout << "Enter the lower bound of integration" << endl;
	cin >> a;
	cout << "Enter the upper bound of integration" << endl;
	cin >> b;
	if (b < a) //если границы введены неверно
	{
		double t = a;
		a = b;
		b = t;
	}
	cout << "Enter the integration accuracy" << endl;
	cin >> precision;
	precision = abs(precision); //на случай отрицательной точности
	double** R; //создаем двумерный массив для сохранения значений площади
	R = new double* [precision - 1];
	for (int i = 0; i < precision; i++)
	{
		R[i] = new double[precision - 1];
	}
	double sum;
	h = b - a;
	for (int i = 0; i < precision; i++) //считаем первые площади трапецией
	{
		sum = 0;
		for (int k = 1; k <= pow(2, i) - 1; k++)
		{
			sum = sum + f(a + k * h);
		}
		R[i][0] = h / 2 * (f(a) + 2 * sum + f(b));
		for (int k = 1; k < i; k++) //экстраполируем, используя другие значения площади
		{
			R[i][k] = R[i][k - 1] + (R[i][k - 1] - R[i - 1][k - 1]) / (pow(4, k) - 1); //Тут от R11
		}
		h = h / 2; //делим шаг на половину
	}
	S = *R[precision - 1, precision - 1];
	cout << S;
	return 0;
}
